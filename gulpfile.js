var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync");
var jade = require("gulp-jade");
var bower = require("gulp-bower");

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
  })
})

gulp.task('sass', function (){
  return gulp.src('app/sass/**/*.sass')
      .pipe(sass())
      .pipe(gulp.dest('app/css'))
      .pipe(browserSync.reload({
        stream: true,
      }))
});

gulp.task('jade', function() {

  return gulp.src(['app/jade/*.jade', '!app/jade/header.jade', '!app/jade/footer.jade'])
    .pipe(jade({
      pretty: true,
    }))
    .pipe(gulp.dest('app/'))
    .pipe(browserSync.reload({
      stream: true,
    }))
});

gulp.task('bower', function() {
  return bower()
    .pipe(gulp.dest('app/bower_components'))
});

gulp.task('watch', ['browserSync', 'sass', 'jade'], function(){
  gulp.watch('app/sass/**/*.sass', ['sass']);
  gulp.watch('app/jade/**/*.jade', ['jade']);
  gulp.watch('bower.json', ['bower']);
});
